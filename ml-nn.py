import numpy
import math
import matplotlib.pyplot as plt

#
# ___Note___ based on sigmoid only 
#
class nnet:
    def sigmoid(self,x):
        return  1 / (1 + math.exp(-x))

    def __init__(self):
        self.input_size = 0;
        self.num_layers = 0;
        self.weights= dict(); # nxm matrix for a layer L and its previous layers dimension  dim(prev) x dim(L)
        self.bias = dict();  # matrix dim(L) X 1 
        self.zstate= dict(); # dim(L) X 1 
        self.astate= dict(); # dim(L) X 1 
        self.delw= dict(); # dim(L) X 1 
        self.delz= dict(); # dim(L) X 1 
        self.avgdelw= dict(); # dim(L) X 1 
        self.avgdelb= dict(); # dim(L) X 1 


    def add_input_layer(self, sizel): # number of input features 
        self.input_size = sizel;
        pass;

    # assume fully connected, activation sigmoid
    def add_hidden_layer(self, number_neurons):
        p = self.bias.keys()
        if (len(p) >0) :
            last_layer_size = len(self.bias[len(p)-1])
        else :
            last_layer_size = self.input_size;

        initw = numpy.random.randn(last_layer_size,number_neurons) + 0.5 
        initb = numpy.zeros(shape=(number_neurons,1)) 
        self.weights[len(p)] = initw;
        self.bias[len(p)] = initb;

    # return  output vector
    def forward_pass(self, input_vec):
        in2arr = numpy.array(input_vec);
        in2arr = numpy.reshape(in2arr,(len(in2arr),1));
        p = self.bias.keys()
        sigmoid_v = numpy.vectorize(self.sigmoid)
        for i in range(len(p)):
            if (i == 0) :
                t =  (numpy.transpose(self.weights[i]));
                self.zstate[i] =  (t.dot(in2arr)) + self.bias[i];

            else :
                self.zstate[i] =  (numpy.transpose(self.weights[i]).dot(self.astate[i-1])) + self.bias[i];

            self.astate[i] =  sigmoid_v(self.zstate[i])  
            #print("RESULT at layer %d"%(i),self.astate[i]);


    def show_result(self):
        p = self.bias.keys()
        print(self.astate[len(p)-1]);

    def show_weights(self):
        p = self.bias.keys()
        for j in range(len(p)):
            l = len(p)-j-1;
            #print("Weights %d"%(l), self.weights[l]);
            #print("Bias %d"%(l), self.bias[l]);


    def backprop(self, input_set, output_set, lr, epochs):
        p = self.bias.keys()
        errplt = list();
        # epoch times 
        lasterr = 0;
        for ep in range(epochs):
            self.avgerr = 0;
            self.avgdelw= dict(); # dim(L) X 1 
            self.avgdelb= dict(); # dim(L) X 1 
            # over all dataset 
            randl = numpy.random.randint(len(input_set)); 
            choices = [randl]; # only 1 sample to be used
            for i in range(len(input_set)):
                #if (randl == 0) :
                #    continue;
                #for i in range(randl):
                #for i in choices:
                in2arr = numpy.array(input_set[i]);
                in2arr = numpy.reshape(in2arr,(len(in2arr),1));
                self.astate[-1] = in2arr; # output before layer 1 is inputs


                self.forward_pass(input_set[i]);
                out2arr = numpy.array(output_set[i]);
                out2arr = numpy.reshape(out2arr,(len(out2arr),1));
                err =  0.5 * numpy.power((self.astate[len(p)-1]-out2arr),2) ; 
                err = numpy.sum(err); # final error at epoch 
                
                self.avgerr += err;
                
                # now over all layers 
                for j in range(len(p)):
                    l = len(p)-j-1;
                    # calculate delw for this layer 
                    if (l == len(p)-1) :
                        self.delz[l] = numpy.atleast_2d((self.astate[l] - out2arr) * (self.astate[l]) * (1-self.astate[l]))  ; 
                    else:
                        self.delz[l] = (self.weights[l+1]).dot(self.delz[l+1])     * self.astate[l] * (1-self.astate[l]); 
                    self.delw[l] =   (numpy.atleast_2d(self.astate[l-1])).dot(numpy.atleast_2d(self.delz[l]).T)

                    if (l in self.avgdelw.keys()) :
                        self.avgdelw[l] = self.avgdelw[l] + self.delw[l];
                        self.avgdelb[l] = self.avgdelb[l] + self.delz[l];
                    else :
                        self.avgdelw[l] = self.delw[l];
                        self.avgdelb[l] = self.delz[l];

            # now at end of epoch 
            self.avgerr /=  len(input_set); 
            print("Error at epoch %d  for sample  is %f"%(ep,  self.avgerr));
            for j in range(len(p)):
                        # stochasitic gradient or regular descent used 
                        if (randl >0) :
                            self.avgdelw[j] = self.avgdelw[j] / len(input_set); # divide by length of set to average 
                            self.avgdelb[j] = self.avgdelb[j] / len(input_set); # divide by length of set to average 
                            #self.avgdelw[j] = self.avgdelw[j] / randl
                            #self.avgdelb[j] = self.avgdelb[j] / randl

                        self.weights[j] = self.weights[j] - (lr *  self.avgdelw[j]);
                        self.bias[j] = self.bias[j] - (lr *  self.avgdelb[j]);
            #print((self.avgerr - lasterr)) 
            if (abs(self.avgerr - lasterr) < 0.00000000001) :
                pass;
            lasterr = self.avgerr;
            errplt.append(self.avgerr);

        plt.plot(errplt); plt.show();


# main 
dataset_in  = [
                  [0, 0],
                  [0, 1],
                  [1, 0],
                  [1, 1],
                  ];


dataset_out  = [
                  [0],
                  [1],
                  [1],
                  [0],
                ];


a = nnet();
a.add_input_layer(2);
a.add_hidden_layer(2);
a.add_hidden_layer(1);
a.backprop(dataset_in, dataset_out, .5, 10000);
a.show_weights()
a.forward_pass([1,1])
a.show_result()
a.forward_pass([1,0])
a.show_result()
a.forward_pass([0,1])
a.show_result()
a.forward_pass([0,0])
a.show_result()
