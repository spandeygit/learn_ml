# simple neural net model 
import math 
import matplotlib.pyplot as plt

inputs = [.9,.8,.7,.5,.1]
outputs = [.1,.2,.3,.5,.9]


def forward_pass(x, w, b):
    net = x* w + b;
    out = 1/(1 + math.exp(-net));
    return [out, net]

    


def back_prop(learning_rate, epochs):
    global inputs, outputs
    w = 1; b = 0;  # starting 
    errplot = []
    
    for i in range (epochs):
        newout = []
        netlist= []
        err = 0;
        for j in range (len(inputs)):
            # we run each data through the machine and get new values   
            res = forward_pass(inputs[j], w, b);
            newout.append(res[0]);
            netlist.append(res[1]);
            err = err + pow((res[0] - outputs[j]),2)/2
        print("Total of error at epoch %d is %f weight is %f bias is %f"%(i,err, w, b));
        errplot.append(err);
            
        # now we are dopne with 1 pass of epoch 
        # now we will back propogate 
        pardev_w = [] 
        pardev_b = [] 
        for j in range (len(inputs)):
            pardev_wi  = (newout[j] - outputs[j]) * newout[j] * (1-newout[j]) * inputs[j]
            pardev_bi  = (newout[j] - outputs[j]) * newout[j] * (1-newout[j]) 
            pardev_w.append(pardev_wi);
            pardev_b.append(pardev_bi);
        new_pardev_w = sum(pardev_w) / len(pardev_w)    
        new_pardev_b = sum(pardev_b) / len(pardev_b)    

        b = b - (learning_rate * new_pardev_b);
        w = w - (learning_rate * new_pardev_w);
        

    plt.plot(errplot);
    plt.show();
        
            
        


    

back_prop(.1,5000);
    
