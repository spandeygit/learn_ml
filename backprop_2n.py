# simple neural net model 
import math 
import matplotlib.pyplot as plt

inputs = [.9,.8,.7,.5,.1]
targets = [.1,.2,.3,.5,.9]


def forward_pass(x, w1, b1, w2,b2):
    net1 = x* w1 + b1;
    out1 = 1/(1 + math.exp(-net1));
    net2 = out1* w2 + b2;
    out2 = 1/(1 + math.exp(-net2));
    return [out2, net2, out1,net1]

    


def back_prop(learning_rate, epochs):
    global inputs, targets
    w1 = 1; b1 = 0;  # starting 
    w2 = 1; b2 = 0;  # starting 
    errplot = []
    
    for i in range (epochs):
        out2list = []
        net2list= []
        out1list = []
        net1list= []

        err = 0;
        for j in range (len(inputs)):
            # we run each data through the machine and get new values   
            res = forward_pass(inputs[j], w1, b1, w2,b2);
            out2list.append(res[0]);
            net2list.append(res[1]);
            out1list.append(res[2]);
            net1list.append(res[3]);

            err = err + pow((res[0] - targets[j]),2)/2

        print("Total of error at epoch %d is %f w1 is %f b1 is %f w2 is %f b2 is %f"%(i,err, w1, b1, w2, b2));
        errplot.append(err);
            
        # now we are dopne with 1 pass of epoch 
        # now we will back propogate 
        pardev_w1 = [] 
        pardev_b1 = [] 
        pardev_w2 = [] 
        pardev_b2 = [] 

        for j in range (len(inputs)):
            pardev_w2i  = (out2list[j] - targets[j]) * out2list[j] * (1-out2list[j]) * out1list[j]
            pardev_b2i  = (out2list[j] - targets[j]) * out2list[j] * (1-out2list[j]) 
            # delE/Delz2 - is a useful tool to use for cascading to previous layer
            del2 = (out2list[j] - targets[j]) * out2list[j] * (1-out2list[j]) 

            pardev_w1i  = del2 * w2 * out1list[j] * (1-out1list[j]) * inputs[j]
            pardev_b1i  = del2 * w2 * out1list[j] * (1-out1list[j]) 
            # delE/Delz1 - is a useful tool to use for cascading to previous layer 
            del1 = del2 * w2 * out1list[j] * (1-out1list[j]) 

            pardev_w2.append(pardev_w2i);
            pardev_b2.append(pardev_b2i);
            pardev_w1.append(pardev_w1i);
            pardev_b1.append(pardev_b1i);

        new_pardev_w2 = sum(pardev_w2) / len(pardev_w2)    
        new_pardev_b2 = sum(pardev_b2) / len(pardev_b2)    
        new_pardev_w1 = sum(pardev_w1) / len(pardev_w1)    
        new_pardev_b1 = sum(pardev_b1) / len(pardev_b1)    

        b1 = b1 - (learning_rate * new_pardev_b1);
        w1 = w1 - (learning_rate * new_pardev_w1);
        b2 = b2 - (learning_rate * new_pardev_b2);
        w2 = w2 - (learning_rate * new_pardev_w2);
        

    plt.plot(errplot);
    plt.show();
        
back_prop(.1,5000);
    
